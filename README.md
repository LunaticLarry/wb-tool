# README #

Sorting tool for Walkbase data files. GNU coreutils are required to be installed on the machine.
The the script is ran by invoking python3 wb-tool.py [arguments]

Arguments:
----------

-i --ifile : (REQUIRED) Path to folder where all the input files are kept.


-o --ofile : (REQUIRED) Path to folder where the sorted data will be stored.


-c --cores : How many cores should be used by GNU Sort? Diminishing returns if greater than 8


-s --chunksize : How large temp files GNU Sort will be working with.

Exit codes:
###########

2 : Error in arguments


3 : Could not find gsort on Mac


4 : Subprocess exited with unusual returncode, check program output


### How do I get set up? ###

* Clone the repo and verify that you have GNU core utils installed on 
your machine.

* Verify that you have a path for both the input and the output folders.

* You need to have write access in the output folder.

### Contact ###

* Anton Lindholm
* anton.c.e.lindholm@gmail.com
* https://keybase.io/acl