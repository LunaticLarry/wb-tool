import datetime
import getopt
import os
import sys
import glob
import gzip
import csv
from multiprocessing import cpu_count
import subprocess
import re

LINES = 100000  # How many lines per output file?
CORES = cpu_count()  # How many cores GNU sort utilizes. Greater than 8 gives diminishing returns.
CHUNK_SIZE = '1G'  # Max size of the chunks GNU sort utilizes


def run_subprocess(args_list, message=None):
    """Runs command described by the args list by invoking a subprocess and then checking the exit code.

        Parameters
        ----------
        args_list : String[]
            Command and parameters to be run by the subprocess module.
        message : String
            Optional message to be printed before command is run.
    """
    if message:
        print(message)
    print(args_list)
    rc = subprocess.run(args_list).returncode
    if rc != 0:
        print("Failed to run: ", args_list,"returncode: " + rc)
        sys.exit(4)


def split_and_rename(unsorted_file, lines):
    """Splits, gzips and renames the files in unsorted site folder. When done the sorted unpacked data is deleted to save
        space.

        Parameters
        ----------
        unsorted_file : String
            Path to files that will be merged.
        lines : Integer
            Describes the how many lines there should be in each output file
    """

    dir_path = os.path.dirname(os.path.realpath(unsorted_file))
    args_list = ['split', '-l', str(lines), unsorted_file, dir_path + os.path.sep +'split']
    run_subprocess(args_list, "split: " + unsorted_file)
    rm_file(unsorted_file)
    split_files = glob.glob(pathname=dir_path + os.path.sep +'split*')
    for file in split_files:

        with open(file, 'rt') as f:
            lastline = ""
            for lastline in f: # Iterate through file to find last line, this needs to be done since we can't be sure
                pass           # that the file contains LINES rows

            packed_filename = datetime.datetime.fromtimestamp(
                    int(lastline.split('\t')[1]),
                    datetime.timezone.utc
                ).strftime('position_data_%Y_%m_%d_%H%M%S')

        args_list = ['mv', file, dir_path + os.path.sep + packed_filename]
        run_subprocess(args_list)
        args_list = ['gzip', dir_path + os.path.sep + packed_filename]
        run_subprocess(args_list, "Packing "+ packed_filename)


def merge_files(site_folder_path):
    """Unpacks and merges csv data files in the folder. Merged files are deleted to save space.
        Parameters
        ----------
        site_folder_path : String
            Path to files that will be merged.
        """
    site_files = glob.glob(pathname=site_folder_path+os.path.sep + 'position_data*.gz')
    for filename in site_files:
        print("Unpacking and merging: "+filename)
        with gzip.open(filename=filename, mode='rt') as f:
            for line in csv.reader(f, dialect="excel-tab"):
                with open(site_folder_path+os.path.sep + "data.unsorted", mode='a') as cs:
                    writer = csv.writer(cs, delimiter='\t')
                    writer.writerow(line)
        rm_file(filename)


def para_sort(unsorted_path, cores, chunk_size):
    """ Sort csv at unsorted_path utilizing GNU sort. Data is sorted by timestamp, device ID, position, floor level
        and accuracy.

        Parameters
        ----------
        unsorted_path : String
            Path to file to be deleted.
        cores : Integer
            Describes how many cores should be used for parallel sorting
        chunk_size : String
            Defines maximum size of the data chunks GNU sort uses when sorting
        """

    if sys.platform == "darwin":
        args_list = ['sort', '-t', '\t', '-k 2,2n', '-k 1,1', '-k 3,3', '-k 5,5', '-S'+chunk_size, '-uo',
                     unsorted_path, '--parallel=' + str(cores), unsorted_path]
    else:
        args_list = ['sort', '-t', '\t', '-k 2,2n', '-k 1,1', '-k 3,3', '-k 5,5', '-S'+chunk_size, '-uo',
                     unsorted_path, '--parallel=' + str(cores), unsorted_path]

    run_subprocess(args_list, "Sorting: " + unsorted_path)


def rm_file(path):
    """Uses rm to delete the file described by the path
        Parameters
        ----------
        path : String
            Path to file to be deleted.
        """
    run_subprocess(['rm', path], "Deleting "+path)


def split_and_sort(input_folder_path, output_folder_path, lines_in_output, cores, chunk_size):
    """Iterates through all input files in the input folder. These files are then split up into new csv files in
        respective site folder.

        The existing files the site folders are then merged with the newly created files and then
        deleted to save space.

        After this the data is sorted, split into smaller files and gzipped to save space.

            Parameters
            ----------
            input_folder_path : String
                Path to input folder
            output_folder_path : String
                Path to output folder
            lines_in_output : Integer
                Describes the how many lines there should be in each output file
            cores : Integer
                Describes how many cores should be used for parallel sorting
            chunk_size : String
                Defines maximum size of the data chunks GNU sort uses when sorting
            """

    input_files = glob.glob(pathname=input_folder_path + os.path.sep + '*.gz')
    for filename in input_files:
        with gzip.open(filename=filename, mode='rt') as f:
            for line in csv.reader(f, dialect="excel-tab"):
                if not os.path.exists(output_folder_path + os.path.sep + "site_" + str(line[3])):
                    os.makedirs(output_folder_path + os.path.sep + "site_" + str(line[3]))
                with open(output_folder_path + os.path.sep + "site_" + str(line[3]) + os.path.sep + "data.unsorted",
                          mode='a') as cs:
                    writer = csv.writer(cs, delimiter='\t')
                    writer.writerow(line)

    site_folders = glob.glob(pathname=output_folder_path + os.path.sep + 'site_*')

    for path in site_folders:
        merge_files(path)

    unsorted_files = glob.glob(pathname=output_folder_path + os.path.sep + '*' + os.path.sep + "data.unsorted")

    for path in unsorted_files:
        para_sort(path, cores, chunk_size)
        split_and_rename(path, lines_in_output)


def check_input_path(path):
    """Verifies that user given inputpath is correct.
        Parameters
        ----------
        path : String
            Path to input folder
        """
    return os.path.isdir(path)


def check_output_path(path):
    """Verifies that user given output path is correct and that the user has write access in the given folder.
        Parameters
        ----------
        path : String
            Path to output folder
        """
    return os.path.isdir(path) and os.access(path=path, mode=os.W_OK)


def main(argv):
    input_folder_path = ''
    output_folder_path = ''
    lines_in_output = LINES
    cores = CORES
    chunk_size = CHUNK_SIZE
    chunk_pattern = re.compile("\d+[KMGTPEZY]$")

    try:
        opts, args = getopt.getopt(argv, "hi:o:c:s:", ["ifile=", "ofile=", "cores=", "chunksize="])

        if(sys.platform == "darwin"):  # If on mac, we need to check for GNU-sort,
                                        # normal Mac sort can't sort in parallel
            subprocess.run(["gsort", '--version'])

    except getopt.GetoptError:
        print('WB-tool.py -i <inputfolder> -o <outputfolder>')
        sys.exit(2)

    except OSError as e:

        if e.errno == os.errno.ENOENT:
            print("GNU sort could not be found")
            print("Please install coreutils using one of these two commands:")
            print("brew install coreutils")
            print("ruby -e \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)\"")
            sys.exit(3)

    for opt, arg in opts:
        if opt == '-h':
            print("Sorting tool for Walkbase data files. GNU coreutils are required to be installed on the machine\n\n"
                  "Arguments:\n"
                  "----------\n\n"
                  "-i --ifile : (REQUIRED) Path to folder where all the input files are kept.\n"
                  "-o --ofile : (REQUIRED) Path to folder where the sorted data will be stored.\n"
                  "-c --cores : How many cores should be used by GNU Sort? Diminishing returns if greater than 8\n"
                  "-s --chunksize : How large temp files GNU Sort will be working with.\n\n"
                  "Exit codes:\n"
                  "###########\n\n"
                  "2 : Error in arguments\n"
                  "3 : Could not find gsort on Mac\n"
                  "4 : Subprocess exited with unusual returncode, check program output\n"
                  )
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_folder_path = arg
        elif opt in ("-o", "--ofile"):
            output_folder_path = arg
        elif opt in ("-c", "--cores"):
            try:
                arg = int(arg)
                if arg > cores:
                    print("Max cores on this machine is: ", cores)
                    print("cores = ", cores)
                else:
                    cores = arg
            except ValueError:
                print("Cores must be an integer")
                sys.exit(2)

        elif opt in ("-s", "--chunksize"):
            if chunk_pattern.match(arg):
                chunk_size = arg
            else:
                print("The chunksize must match \d+[KMGTPEZY]$")
                print("ie. 1G")
                sys.exit(2)

    if not check_input_path(input_folder_path):
        print('WB-tool.py -i <inputfolder> -o <outputfolder>')
        print('Check the input path!')
        sys.exit(2)

    elif not check_output_path(output_folder_path):
        print('WB-tool.py -i <inputfolder> -o <outputfolder>')
        print('Check the output path, and verify that you have Write Access!')
        sys.exit(2)

    else:
        input_folder_path = os.path.abspath(input_folder_path)
        output_folder_path = os.path.abspath(output_folder_path)

        split_and_sort(input_folder_path, output_folder_path, lines_in_output, cores, chunk_size)


if __name__ == "__main__":
    main(sys.argv[1:])
